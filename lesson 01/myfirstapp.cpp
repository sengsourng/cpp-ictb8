// Example program
#include <iostream>
#include <string>
using namespace std;

int main()
{
 string name;
 int age;
 
 cout<<"Enter Name:"; getline(cin,name);
 cout<<"Enter Age:"; cin>>age;
 
 cout<<"Hi, "<<name<<endl;
 cout<<"Are you "<<age<<" years old?"<<endl;
  
  return (0);  
}