#include<iostream>
using namespace std;
class Laptop{
	private:
		int code;
		string name;
		int RAM;
		int HDD;
	public:
		void enterData(){
			cout<<"Enter ID:"; cin>>this->code;
			cout<<"Enter Name:"; cin>>this->name;
			cout<<"Enter RAM:"; cin>>this->RAM;
			cout<<"Enter HDD:"; cin>>this->HDD;
		}
		void getData(){
			cout<<"ID:"<<this->code<<endl;
			cout<<"Name:"<<this->name<<endl;
			cout<<"RAM:"<<this->RAM<<endl;
			cout<<"HDD:"<<this->HDD<<endl;
		}
		
		int findMax(int a,int b){
			return (a>b)?a:b;
		}
};


int main(){
	Laptop pc1;
	Laptop pc2;
	
	pc1.enterData();
	pc2.enterData();
		
	pc1.getData();
	cout<<"-------------------------\n";
	pc2.getData();
	cout<<"-------------------------\n";
	cout<<"Max 1:"<<pc1.findMax(3,9);
	cout<<"Max 2:"<<pc2.findMax(10,3);	
		
	return(0);
}